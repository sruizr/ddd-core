#!/usr/bin/env bash

poetry run pytest \
    -W ignore::DeprecationWarning \
    --cov=ddd \
    --cov-report=xml \
    --cov-report=term \
    --cov-report=html \
    tests

    # --cov-config=.coveragerc \
