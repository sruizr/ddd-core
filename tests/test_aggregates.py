import pytest

from ddd.aggregates import Aggregate, ChangeEvent, Entity, ExceptionEvent, Id

from . import EventObserver


class FakeId(Id):
    ...


class FakeExceptionEvent(ExceptionEvent):
    ex_message: str

    def message(self, aggregate) -> str:
        return f"{self.ex_message} from {aggregate.__class__.__name__}"


class An_Id:
    def should_be_created_randomly(self):
        id = Id.create_randomly()
        other_id = Id.create_randomly()
        same_id = Id(id.uuid)

        assert id == same_id
        assert id != other_id

    def should_be_created_from_key(self):
        id = Id.create_from_key("one")
        other_id = Id.create_from_key("one/two")
        same_id = Id.create_from_key("one")
        fake_id = FakeId.create_from_key("one")

        assert id == same_id
        assert id != other_id
        assert id != fake_id


class FakeEntity(Entity):
    id: int
    int_value: str
    str_value: str
    list_value: list

    def change_something(self, id, int_value, str_value, list_value):
        self._id = id
        self._int_value = int_value
        self._str_value = str_value
        self._list_value = list_value


class A_Entity:
    def given_an_entity_with_changes(
        self, id=1, int_value=100, str_value="value", list_value=None
    ):
        entity = FakeEntity()
        entity.change_something(id, int_value, str_value, list_value)
        return entity

    def should_publish_its_attributes(self):
        entity = self.given_an_entity_with_changes()

        assert entity.id == 1
        assert entity.int_value == 100
        assert entity.str_value == "value"
        assert entity.list_value == None

    def should_have_always_an_identificator_id_declared(self):
        class WrongEntity(Entity):
            par: str

        with pytest.raises(AttributeError):
            WrongEntity()

    def should_use_id_for_know_if_equals(self):
        entity = self.given_an_entity_with_changes(id=1, int_value=10)
        same_entity = self.given_an_entity_with_changes(id=1, int_value=1000)
        other_entity = self.given_an_entity_with_changes(id=2, int_value=10)

        assert entity == same_entity
        assert entity != other_entity

    def should_have_a_hash(self):
        entity = self.given_an_entity_with_changes(id=1, int_value=10)
        same_entity = self.given_an_entity_with_changes(id=1, int_value=1000)
        other_entity = self.given_an_entity_with_changes(id=2, int_value=10)

        assert hash(entity) == hash(same_entity)
        assert hash(entity) != hash(other_entity)


class FakeAggregate(Aggregate):
    id: int
    parameter: str
    key: str
    default: str = "default"

    class FakeEvent(ChangeEvent):
        par: int

        def apply_on(self, aggregate):
            aggregate._parameter = self.par

    def __init__(self):
        super().__init__()
        self._key = "aggregate-key"

    def change_parameter(self, par: str):
        self.apply_change(self.FakeEvent(par))

    def method_with_exception_event(self, ex_message):
        self.notify_event(FakeExceptionEvent(ex_message))


class An_Aggregate:
    def should_notify_events(self):
        aggregate = FakeAggregate()
        observer = EventObserver()
        aggregate.add_event_observer(observer.notify)

        aggregate.change_parameter("value")

        assert (aggregate.FakeEvent("value"), aggregate) in observer.events

    def should_apply_changes_in_aggregate(self):
        aggregate = FakeAggregate()
        assert aggregate.parameter is None

        aggregate.change_parameter("value")

        assert aggregate.parameter == "value"

    def should_have_default_fields(self):
        aggregate = FakeAggregate()

        assert aggregate.default == "default"

    def should_keep_events_to_new_observers(self):
        aggregate = FakeAggregate()

        aggregate.change_parameter("value")
        aggregate.change_parameter("other-value")

        new_observer = EventObserver()
        aggregate.add_event_observer(new_observer.notify)

        assert len(new_observer.events) == 2

    def should_notify_exception_event(self):
        aggregate = FakeAggregate()
        observer = EventObserver()
        aggregate.add_event_observer(observer.notify)

        aggregate.method_with_exception_event("something-crashed")

        assert len(observer.events) == 1
        event, sender = observer.events[0]
        assert event.message(aggregate) == "something-crashed from FakeAggregate"
        assert event.level == 5
        assert sender == aggregate

    def should_notify_only_exception_events(self):
        aggregate = FakeAggregate()
        observer = EventObserver()
        aggregate.add_event_observer(observer.notify, 5)

        aggregate.change_parameter("other")
        aggregate.method_with_exception_event("something-crashed")

        assert len(observer.events) == 1
        event, _ = observer.events[0]
        assert event.level == 5

    def should_not_notify_twice_to_the_same_observer(self):
        aggregate = FakeAggregate()
        observer = EventObserver()
        aggregate.add_event_observer(observer.notify)
        aggregate.add_event_observer(observer.notify)

        aggregate.change_parameter("other")

        assert len(observer.events) == 1


class FakeEvent(ExceptionEvent):
    ...


def test_event_default_message():
    aggregate = FakeAggregate()

    event = FakeEvent()
    assert event.message(aggregate) == "FakeEvent@FakeAggregate[aggregate-key]"
