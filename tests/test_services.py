from unittest.mock import MagicMock

import pytest

from ddd.aggregates import ExceptionEvent, WarningEvent
from ddd.services import Command, CommandFactory, Query, Service


@pytest.mark.asyncio
async def test_a_command_is_abstract():
    command = Command()
    with pytest.raises(NotImplementedError):
        await command.execute(MagicMock())


@pytest.mark.asyncio
async def test_a_query_is_abstract():
    query = Query()
    with pytest.raises(NotImplementedError):
        await query.fetch(MagicMock())


class FakeService(Service):
    def __init__(self, event_bus_client=None):
        super().__init__(event_bus_client)
        self.driven = MagicMock()
        self.driven.fetch.side_effect = lambda x: x


class OtherFakeService(Service):
    ...


class FakeCommandCommand(Command):
    cmd_par: int

    async def execute(self, service: FakeService) -> None:
        service.driven.execute(self.cmd_par)


class FakeQueryQuery(Query):
    qry_par: str

    async def fetch(self, service: FakeService):
        return service.driven.fetch(self.qry_par)


class OrphanQuery(Query):
    qry_par: str

    async def fetch(self, service: OtherFakeService) -> None:
        return


class A_Service:
    @pytest.mark.asyncio
    async def should_execute_command(self):
        service = FakeService()
        await service.execute("fake_command", 1)

        service.driven.execute.assert_called_with(1)

    def should_supply_available_commands(self):
        service = FakeService()

        assert service.all_command_names == [
            "fake_command",
        ]
        assert service.all_query_names == ["fake_query"]

    @pytest.mark.asyncio
    async def should_fetch_query(self):
        service = FakeService()
        assert await service.fetch("fake_query", qry_par="string") == "string"

        service.driven.fetch.assert_called_with("string")

    @pytest.mark.asyncio
    async def should_raise_exception_if_no_compatible_query(self):
        service = FakeService()

        with pytest.raises(ValueError) as e:
            await service.fetch("orphan", qry_par="1")

        assert (
            str(e.value) == '"orphan" query is not available for service "FakeService"'
        )

    def should_run_sync_commands(self):
        service = FakeService()
        service.execute_syncronous("fake_command", 1)

        service.driven.execute.assert_called_with(1)

    def should_run_sync_queries(self):
        service = FakeService()
        assert service.fetch_syncronous("fake_query", qry_par="string") == "string"

        service.driven.fetch.assert_called_with("string")


def test_a_warning_event_has_level_4():
    assert WarningEvent().level == 4


def test_a_exception_event_has_level_5():
    assert ExceptionEvent().level == 5


class A_CommandFactory:
    def should_load_commands_of_a_service(self):
        fry = CommandFactory(FakeService, validate=True)

        command = fry.create_command("fake_command", 12)

        assert type(command) is FakeCommandCommand
        assert command.cmd_par == 12

    def should_load_queries_of_a_service(self):
        fry = CommandFactory(FakeService, validate=True)

        query = fry.create_query("fake_query", qry_par="a")

        assert type(query) is FakeQueryQuery
        assert query.qry_par == "a"

    def should_validate_command_inputs(self):
        fry = CommandFactory(FakeService, validate=True)

        with pytest.raises(TypeError):
            fry.create_command("fake_command", None)

    def should_detect_incorrect_command_names(self):
        fry = CommandFactory(FakeService, validate=True)

        with pytest.raises(ValueError):
            fry.create_command("no_exist_command", None)

    def should_check_if_is_command_or_query(self):
        fry = CommandFactory(FakeService, validate=True)

        with pytest.raises(ValueError):
            fry.create_command("fake_query", None)

        with pytest.raises(ValueError):
            fry.create_query("fake_command")
