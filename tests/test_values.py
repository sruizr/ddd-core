from typing import Literal

import pytest

from ddd.values import Value


class FakeValue(Value):
    int_var: int
    str_var: str = "default"
    obj_var: str | None


class FakeValue2(FakeValue):
    new_par: int
    literal_var: Literal["one", "two"] | None = "one"


class A_Value:
    def should_be_initialized(self):
        val = FakeValue(1, "str", None)

        assert val.int_var == 1
        assert val.str_var == "str"
        assert val.obj_var == None

    def should_be_inmutable(self):
        val = FakeValue(1, "str", None)

        with pytest.raises(AttributeError):
            val.int_var = 2

    def should_be_equal_if_same_invariants(self):
        val = FakeValue(1, "str", None)
        other = FakeValue(1, "str", None)

        assert val == other

    def should_not_be_equal_with_other_invariants(self):
        val = FakeValue(1, "str", None)
        other = FakeValue(2, "str", None)

        assert val != other

    def should_return_values_as_dict(self):
        value = FakeValue(1, "str", None)

        assert value.as_dict() == {"int_var": 1, "str_var": "str", "obj_var": None}

    def should_keep_parameters_of_parent_class(self):
        val = FakeValue2(1, "str", None, 4)

        assert val.int_var == 1
        assert val.str_var == "str"
        assert val.obj_var == None
        assert val.new_par == 4

    def should_validate_parameters(self):
        assert FakeValue2(1, "str", None, 4, _validate=True)

        with pytest.raises(TypeError):
            FakeValue2(1, "str", None, 4, literal_var=10, _validate=True)

    def should_not_allow_wrong_fields(self):
        with pytest.raises(AttributeError) as e:
            FakeValue(1, "one", "str", new_field=1, _validate=True)

    def should_have_default_values(self):
        val = FakeValue(1, obj_var="obj")

        assert val.str_var == "default"
        assert val.int_var == 1
        assert val.obj_var == "obj"
