from ddd.aggregates import Aggregate, Event


class EventObserver:
    def __init__(self):
        self.events = []

    def notify(self, event: Event, aggregate: Aggregate):
        self.events.append((event, aggregate))
